# DESCRIPTION #

A more sophisticated example. Brings together drawing a sprite (with transparent background,) and the game loop to introduce moving a sprite with the arrow keys. 

# INSTRUCTIONS #
Not written yet. 

# CREDITS #
Loosely based on tutorials from [Lazy Foo](http://lazyfoo.net/SDL_tutorials/) used with permission. 
