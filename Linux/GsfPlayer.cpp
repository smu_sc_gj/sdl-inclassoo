#include "GsfPlayer.h"
#include "GsfImage.h"


GsfPlayer::GsfPlayer()
{
	setPosition(0, 0);
	setSpeedX(0);
	setSpeedY(0);
}

GsfPlayer::~GsfPlayer()
{
	SDL_FreeSurface(spriteSheet);
}

void GsfPlayer::init()
{
	this->spriteSheet = GsfImage::loadImage("mechwarrior.bmp", 0xFF, 0x0, 0xFF);
	
	this->standingClip.x = 0;
	this->standingClip.y = 0;
	this->standingClip.w = 87;
	this->standingClip.h = 80;
}

void GsfPlayer::setPosition(int x, int y)
{
	this->position.x = x;
	this->position.y = y;
}

void GsfPlayer::setSpeedX(int x)
{
	this->speedX = x;
}

void GsfPlayer::setSpeedY(int y)
{
	this->speedY = y;
}

void GsfPlayer::draw(SDL_Surface* screen)
{
	//Blit the sprite to the screen
	SDL_BlitSurface(this->spriteSheet, &this->standingClip, screen, &this->position);
}
void GsfPlayer::update(float scalar)
{
	this->position.x += this->speedX * scalar;
	this->position.y += this->speedY * scalar;
}
