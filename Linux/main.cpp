/*
  Based on http://lazyfoo.net/SDL_tutorials/
  Used with permission. 
*/

#include "GsfSDL.h"
#include "GsfImage.h"
#include "GsfPlayer.h"

const int SCREEN_WIDTH=640; //Screen width
const int SCREEN_HEIGHT=480; //Screen height
const int SCREEN_BPP=32;    //Screen bits per pixel

const int FRAME_RATE=16; //our desired frame rate
                         //approx. (1/60)*1000 - 60fps.
int main( int argc, char* args[] )
{
    //Event driven programming variables
    SDL_Event event; //to store the event from SDL
    int quit = 1;  //keep the game loop running (starts at false).

    //Keystates array (for the state of all keys)
    Uint8 *keystates;

    //timer variables
    float scalar;
    Uint32 startTime;
    Uint32 currentTime;
    Uint32 delta;

    //Declare pointers to surface structures
    SDL_Surface* screen = NULL;
  
   
    SDL_Surface* background = NULL;

    //Initialise SDL
    //NOTE: SDL_INIT_EVERYTHING is a constant defined by SDL
    SDL_Init( SDL_INIT_EVERYTHING );

    //Surface structure we'll use for the window - screen
    //This is essentially the back buffer. 
    screen = SDL_SetVideoMode( SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_BPP, SDL_SWSURFACE );

     //Load our bitmap image
	background = GsfImage::loadImage("background.bmp");

	GsfPlayer* player = new GsfPlayer();
	player->init();
	player->setPosition(20, 20);

    //Initlaise the timer
    startTime = SDL_GetTicks();

    //Event loop
    while(quit == 1)
    {
        //If there's events to handle 
        if( SDL_PollEvent( &event ) ) 
        { 
            switch(event.type) 
            { 
                case SDL_QUIT: //If the user has closed(x) out the window 
                    //Quit the program 
                    quit = 0;
                break;  
            }
        }

        //Check time - work out scalar
        currentTime = SDL_GetTicks();
        Uint32 delta = currentTime - startTime;

        if(delta > FRAME_RATE)
        {
           // Reset for next time
           startTime = currentTime;

           //Calculate scalar - will be 1 if we're dead on the 
           //frame rate or some fraction of if we're over/under
           scalar = delta / (FRAME_RATE*1.0f);

           //update sprites use scalar (time delta in unity) to smooth animation
           
           //Get the keystates
           keystates = SDL_GetKeyState( NULL );
           
           if(keystates[SDLK_RIGHT])
           {
			   player->setSpeedX(1);
           }
		   else
		   {
			   player->setSpeedX(0);
		   }

		   if (keystates[SDLK_DOWN])
		   {
			   player->setSpeedY(1);
		   }
		   else
		   {
			   player->setSpeedY(0);
		   }
        
		   player->update(scalar);

           //draw sprites (e.g. player)
           //Blit the background to the screen
	       //Draw to backbuffer
	       SDL_BlitSurface( background, NULL, screen, NULL );

	      player->draw(screen);

           //Refresh the screen (replace with backbuffer). 
           SDL_Flip( screen );
        }
        else
        {
            //A delay of zero is enough to put the process back into 
            //the ready queue.
            SDL_Delay(0);   
        }
    
    }

    SDL_FreeSurface(background);
    
	delete player;

    //Shutdown SDL - clear up resorces etc. 
    SDL_Quit();

    return 0;
}
