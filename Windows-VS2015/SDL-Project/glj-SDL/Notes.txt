** Homebrew SDL **

SDL 1.2 is not being activly developed and as a result there is no supplied version fro visual studio 2010 onwards.  Visual Studio 2015 uses a slightly modified vcr (visual C runtime), the result is that using the old 2010 dlls breaks due to linker issues. 

This also affects the Nuget version which is strangly still available in its broken form.

The solution is to rebuild the SDL libraries from source and produce a version which is correctly linked with the new vc runtime. 

Refs:
https://stackoverflow.com/questions/30366552/error-lnk2001-imp-fprintf-visual-studio-2015-rc
https://stackoverflow.com/questions/30412951/unresolved-external-symbol-imp-fprintf-and-imp-iob-func-sdl2

Glenn (16-2-2018)
Report issues to: gljenkins@cardiffmet.ac.uk