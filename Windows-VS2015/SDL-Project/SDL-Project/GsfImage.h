#pragma once

#include "GsfSDL.h"

class GsfImage
{
public:
	GsfImage();
	~GsfImage();

	static SDL_Surface* loadImage(char* name);
	static SDL_Surface* loadImage(char* name, Uint8, Uint8, Uint8);
};

