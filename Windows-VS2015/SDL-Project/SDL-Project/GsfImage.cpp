#include "GsfImage.h"



GsfImage::GsfImage()
{
}


GsfImage::~GsfImage()
{
}

SDL_Surface* GsfImage::loadImage(char* name)
{
	SDL_Surface* optimised = NULL;
	SDL_Surface* rawImage = SDL_LoadBMP(name);

	if (rawImage != NULL) //if the image is not null
	{
		//Optimise
		optimised = SDL_DisplayFormat(rawImage);

		//Dispose of rawImage - we have our optimised copy
		SDL_FreeSurface(rawImage);
	}
	else
	{
		printf("Failed to load image");
		exit(1);
	}

	return optimised;
}

SDL_Surface* GsfImage::loadImage(char* name, Uint8 r, Uint8 g, Uint8 b)
{
	SDL_Surface* optimised = NULL;
	SDL_Surface* rawImage = SDL_LoadBMP(name);

	if (rawImage != NULL) //if the image is not null
	{
		//Optimise
		optimised = SDL_DisplayFormat(rawImage);

		//Create an SDL unsigned integer to represent our colour
		//I'm using cyan (pink) 255,0,255 or 0xFF 0x0 0xFF in hexadecimal
		Uint32 colourkey = SDL_MapRGB(optimised->format, r, g, b);

		SDL_SetColorKey(optimised, SDL_SRCCOLORKEY, colourkey);

		//Dispose of rawImage - we have our optimised copy
		SDL_FreeSurface(rawImage);
	}
	else
	{
		printf("Failed to load image");
		exit(1);
	}

	return optimised;
}
