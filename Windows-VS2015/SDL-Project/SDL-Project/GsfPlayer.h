#pragma once

#include "GsfSDL.h"

class GsfPlayer
{
private:
	//Blit to here
	SDL_Rect position;

	//Blit from here in the sprite sheet
	SDL_Rect standingClip;

	//Sprite sheet
	SDL_Surface* spriteSheet;

	int speedX;
	int speedY;

public:
	GsfPlayer();
	~GsfPlayer();

	void init();
	void setPosition(int x, int y);

	void setSpeedX(int);
	void setSpeedY(int);

	void draw(SDL_Surface* screen);
	void update(float scalar);
};

